extern crate blake2;

use blake2::{Blake2b, Digest};

pub fn hash_string(data: &str) -> String {
    let mut hasher = Blake2b::new();
    hasher.input(data.as_bytes());
    let hash = hasher.result();
    format!("{:x}", hash)
}

mod tree {
    struct Node {
        value: String,
        left_child: Option<Node>,
        right_child: Option<Node>,
    }

    pub fn add() -> String {
        print_tree()
    }

    pub fn print_tree() -> String {
        "".to_string()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn it_generates_a_content_hash() {
        let generated = hash_string("Hello, world!");
        let expected = "a2764d133a16816b5847a737a786f2ece4c148095c5faa73e24b4cc5d666c3e45ec271504e14dc6127ddfce4e144fb23b91a6f7b04b53d695502290722953b0f";
        assert_eq!(generated, expected);
    }
}
